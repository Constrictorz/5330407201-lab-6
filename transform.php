<?php
		//Name
        $xslFile = "SummaryBlogs.xsl";
        $xmlFile = "SummaryBlogs.xml";
		
		//Create Doc
        $doc = new DOMDocument();
        $xsl = new XSLTProcessor();
		
		//Load & Process
        $doc->load($xslFile);
        $xsl->importStyleSheet($doc);
        $doc->load($xmlFile);
        
        echo $xsl->transformToXML($doc);      
?>