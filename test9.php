<?php // test9.php
$doc = new DOMDocument;
$doc->load('nations2.xml');
// create new nation element
$nationElem = $doc->createElement('nation');
$nationElem->setAttribute('id', 'jp');
// get the root element of an XML document (element �nations�)
$nations = $doc->documentElement;
// add the new nation element as a child of element �nations�
$nations->appendChild($nationElem);
// create element name and the text node to contain its value
$name = $doc->createElement('name');
$nameValue = $doc->createTextNode('Japan');
$name->appendChild($nameValue);
// create element location and its text node to contain its value
$loc = $doc->createElement('location');
$locValue = $doc->createTextNode('East Asia');
$loc->appendChild($locValue);
// append name and location to the new element nation
$nationElem->appendChild($name);
$nationElem->appendChild($loc);
// save the modified content in file �nations3.xml�
$doc->save('nations3.xml');
?>