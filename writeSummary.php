<?php 

        $doc = new DOMDocument;
        $dom = new DomDocument('1.0','UTF-8'); 
        $doc->load('http://www.gotoknow.org/blogs/posts?format=rss');

        $rss = $doc->documentElement;
        $element = $rss->getElementsByTagName('item');

        echo "Reading from summaryBlogs.xml...<br>";
        $items = $dom->appendChild($dom->createElement('items'));

        foreach ($element AS $item) {
		
                $getTitle = $item->getElementsByTagName("title")->item(0)->nodeValue;
                $getLink = $item->getElementsByTagName("link")->item(0)->nodeValue;
                $getAuthor = $item->getElementsByTagName("author")->item(0)->nodeValue;
                
                $item = $items->appendChild($dom->createElement('item')); 

                $title = $item->appendChild($dom->createElement('title')); 
                $title->appendChild($dom->createTextNode($getTitle));
				
                $link = $item->appendChild($dom->createElement('link'));
                $link->appendChild($dom->createTextNode($getLink));
				
                $author = $item->appendChild($dom->createElement('author')); 
                $author->appendChild($dom->createTextNode($getAuthor));

                echo $getLink . $getAuthor;
        }

        $dom->formatOutput = true;
        $str = $dom->saveXML();
        $dom->save('summaryBlogs.xml'); 

?>